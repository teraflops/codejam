//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  BMHTTPSessionManager.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "BMHTTPSessionManager.h"

static BMHTTPSessionManager* _sessionManager;
static NSString* _password;
static NSString* _username;

@implementation BMHTTPSessionManager

+ (BMHTTPSessionManager*)sharedManager
{
    if (_sessionManager == nil) {
        _sessionManager = [[BMHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://teraflops-codejam.cloudapp.net:8090"]];
        if ((_username != nil) && (_password != nil)) {
            [_sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
            [_sessionManager.requestSerializer setAuthorizationHeaderFieldWithUsername:_username password:_password];
        }
    };
    
    return _sessionManager;
}

+ (void)setUsername:(NSString*)username
{
    _sessionManager = nil;
    _username = username;
}

+ (void)setPassword:(NSString*)password
{
    _sessionManager = nil;
    _password = password;
}

+ (NSString*)username
{
    return _username;
}

@end
