//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  BMStoreService.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "BMStoreService.h"
#import "JNKeychain.h"

static NSString* usernameKey = @"username";
static NSString* passwordKey = @"password";

@implementation BMStoreService

+ (void)setUsername:(NSString*)username
{
    [JNKeychain saveValue:username forKey:usernameKey];
}

+ (void)setPassword:(NSString*)password
{
    [JNKeychain saveValue:password forKey:passwordKey];
}

+ (NSString*)username
{
    return [JNKeychain loadValueForKey:usernameKey];
}

+ (NSString*)password
{
    return [JNKeychain loadValueForKey:passwordKey];
}

@end
