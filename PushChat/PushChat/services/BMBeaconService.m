//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  BMBeaconService.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/06
//

#import "BMBeaconService.h"
#import <UIKit/UIKit.h>

@interface BMBeaconService ()

@property (nonatomic, strong, readwrite) UILocalNotification* localNotification;
@property (strong, nonatomic) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation BMBeaconService


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager requestWhenInUseAuthorization];
        [self initRegion];
        [self locationManager:self.locationManager didStartMonitoringForRegion:self.beaconRegion];
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

- (void)initRegion {
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"F7826DA6-4FA2-4E98-8024-BC5B71E0893E"];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"Y1sk"];
    self.beaconRegion.notifyEntryStateOnDisplay = YES;
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"Entered Region");
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"Left Region");
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    CLBeacon *beacon = [[CLBeacon alloc] init];
    beacon = [beacons lastObject];
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    if (self.localNotification != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:self.localNotification];
        self.localNotification = nil;
    }
    
    if (state == CLRegionStateInside) {
        self.localNotification = [[UILocalNotification alloc]init];
        [self.localNotification setAlertBody:@"Arrived at coffee shop"];
        [self.localNotification setFireDate:[NSDate date]];
        [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:self.localNotification]];
    }
    else {
        self.localNotification = [[UILocalNotification alloc]init];
        [self.localNotification setAlertBody:@"Sorry to see you go"];
        [self.localNotification setFireDate:[NSDate date]];
        [[UIApplication sharedApplication] setScheduledLocalNotifications:[NSArray arrayWithObject:self.localNotification]];
    }
}


@end

