//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  BMApiService.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "BMApiService.h"

@implementation BMApiService

+ (void)submitDeviceToken:(NSString*)deviceToken success:(void (^)())successBlock failure:(void (^)())failureBlock
{
    deviceToken = [[deviceToken componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<> "]] componentsJoinedByString:@""];
    
    [[BMHTTPSessionManager sharedManager] PUT:[NSString stringWithFormat:@"/app/rest/users/%@?deviceToken=%@", [BMHTTPSessionManager username], deviceToken] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Response: %@", responseObject);
        successBlock();
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Request failed %@", error);
        failureBlock();
    }];
}

+ (void)getUser:(NSString*)username success:(void (^)(NSDictionary* response))successBlock failure:(void (^)())failureBlock
{
    [[BMHTTPSessionManager sharedManager] GET:[NSString stringWithFormat:@"/app/rest/users/%@", [BMHTTPSessionManager username]] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Response: %@", responseObject);
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Request failed %@", error);
        failureBlock();
    }];
}

+ (void)getOrderWithStatus:(NSString*)status invert:(BOOL)invert success:(void (^)(NSDictionary* response))successBlock failure:(void (^)())failureBlock
{
    [[BMHTTPSessionManager sharedManager] GET:[NSString stringWithFormat:@"/app/rest/users/%@/order/%@?invert=%@", [BMHTTPSessionManager username], status, invert ? @"true" : @"false" ] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Response: %@", responseObject);
        successBlock(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Request failed %@", error);
        failureBlock();
    }];
}

@end
