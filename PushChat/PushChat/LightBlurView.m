//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  LightBlurView.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "LightBlurView.h"

@implementation LightBlurView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self blurView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self blurView];
    }
    return self;
}

- (void)blurView
{
    UIBlurEffect *blurEffect2 = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurEffectView2 = [[UIVisualEffectView alloc] initWithEffect:blurEffect2];
    [blurEffectView2 setFrame:self.bounds];
    [self insertSubview:blurEffectView2 atIndex:1];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
