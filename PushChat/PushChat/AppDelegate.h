//
//  AppDelegate.h
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05.
//  Copyright (c) 2014 Teraflops. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readwrite) NSString* deviceToken;

@end

