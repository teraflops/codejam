//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  LoginViewController.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "LoginViewController.h"
#import "BMHTTPSessionManager.h"
#import "BMApiService.h"
#import "BMStoreService.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIView *redView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) UIActivityIndicatorView *spinner ;

@end

const float BLUR_BOX_INSET = 40.0f;
const float BLUR_BOX_VERTICAL_OFFSET = 140.0f;


@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    // TextField blur boxes
//    UIBlurEffect *blurEffect1 = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIVisualEffectView *blurEffectView1 = [[UIVisualEffectView alloc] initWithEffect:blurEffect1];
//    [blurEffectView1 setFrame:self.usernameTextField.frame];
//    [self.view insertSubview:blurEffectView1 atIndex:1];
//    UIBlurEffect *blurEffect2 = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIVisualEffectView *blurEffectView2 = [[UIVisualEffectView alloc] initWithEffect:blurEffect2];
//    [blurEffectView2 setFrame:self.redView.bounds];
//    [self.redView insertSubview:blurEffectView2 atIndex:1];
    
    // Main blur box
//    float width = self.view.frame.size.width;
//    float height = self.view.frame.size.height;
//    UIBlurEffect *blurEffect3 = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVisualEffectView *blurEffectView3 = [[UIVisualEffectView alloc] initWithEffect:blurEffect3];
//    CGRect dialogBlur = CGRectMake(BLUR_BOX_INSET,
//                                   BLUR_BOX_INSET + BLUR_BOX_VERTICAL_OFFSET,
//                                   (width       - (BLUR_BOX_INSET*2)),
//                                   ((height-(BLUR_BOX_VERTICAL_OFFSET*2)) - (BLUR_BOX_INSET*2)) - 40);
//    [blurEffectView3 setFrame:dialogBlur];
//    [self.view insertSubview:blurEffectView3 atIndex:1];
//    
//    //continue red box
//    UIView* redView = [[UIView alloc] initWithFrame:CGRectMake(BLUR_BOX_INSET,
//                                                              BLUR_BOX_INSET,
//                                                              (width - (BLUR_BOX_INSET*2)),
//                                                               40)];
//    redView.backgroundColor = [UIColor redColor];
//    
//    [self.view insertSubview:blurEffectView3 atIndex:1];
}

- (IBAction)loginTap:(id)sender {
    
    [self.loginButton setTitle:@"" forState:UIControlStateNormal];
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.spinner startAnimating];
    self.spinner.frame = self.loginButton.bounds;
    self.spinner.color = [UIColor whiteColor];
    [self.loginButton addSubview:self.spinner];
    self.loginButton.enabled = NO;
    
    [BMHTTPSessionManager setPassword:self.passwordTextField.text];
    [BMHTTPSessionManager setUsername:self.usernameTextField.text];
    
    [BMApiService getUser:self.usernameTextField.text success:^(NSDictionary *response) {
        [BMStoreService setUsername:self.usernameTextField.text];
        [BMStoreService setPassword:self.passwordTextField.text];
        [self performSegueWithIdentifier:@"webSegue" sender:self];
    } failure:^{
        [self showError];
    }];
}

- (void)showError
{
    [UIView animateWithDuration:0.8f animations:^{
        [self.spinner removeFromSuperview];
        [self.loginButton setTitle:@"Invalid Credentials" forState:UIControlStateNormal];
        [self performSelector:@selector(hideError) withObject:self afterDelay:2];
    }];
}

- (void)hideError
{
    [UIView animateWithDuration:0.8f animations:^{
        [self.loginButton setTitle:@"Login" forState:UIControlStateNormal];
        self.loginButton.enabled = YES;
    }];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}



@end
