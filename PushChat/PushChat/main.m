//
//  main.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05.
//  Copyright (c) 2014 Teraflops. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
