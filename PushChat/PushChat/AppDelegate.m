//
//  AppDelegate.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05.
//  Copyright (c) 2014 Teraflops. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityLogger.h"
#import "BMApiService.h"
#import "BMStoreService.h"
#import "InitialViewController.h"
#import "BMBeaconService.h"

@interface AppDelegate ()

@property (nonatomic, strong, readwrite) NSDictionary* launchOptions;
@property (nonatomic, strong, readwrite) BMBeaconService* beaconService;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Let the device know we want to receive push notifications
    
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
    
    self.beaconService = [[BMBeaconService alloc] init];
    
    [BMStoreService setUsername:nil];
    [BMStoreService setPassword:nil];
    
    UIUserNotificationSettings* settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    if (launchOptions != nil) {
        // Launched from push notification
        self.launchOptions = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    }
    else {
        self.launchOptions = nil;
    }
    
    return YES;
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Received remote notification: %@", userInfo);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newOrder" object:self];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);

    [self completeInit:[deviceToken description]];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    
    [self completeInit:@""];
}


- (void)completeInit:(NSString*)deviceToken
{
    self.deviceToken = deviceToken;
    
    NSString* username = [BMStoreService username];
    NSString* password = [BMStoreService password];
    
    if ((username != nil) && (password != nil)) {
        // User is logged in
        
        [BMHTTPSessionManager setUsername:username];
        [BMHTTPSessionManager setPassword:password];
        
        [BMApiService submitDeviceToken:[deviceToken description] success:^(NSDictionary* response){
            
            NSLog(@"submitDeviceToken SUC");
            //Go to web
            [self goToWeb];
        } failure:^{
            
            NSLog(@"submitDeviceToken FAL");
            // Go to login
            [self goToLogin];
        }];
        
        if (self.launchOptions != nil) {
            // Go to order page
            [self goToOrder];
        }
        else {
            // Check if pending order
            [self checkIfPendingOrder];
        }
    }
    else {
        // User is not logged in. Registration page
        [self goToLogin];
    }
}

- (void)checkIfPendingOrder
{
    [BMApiService getOrderWithStatus:@"COLLECTED" invert:YES success:^(NSDictionary *response) {
        if ([response count] > 0) {
            [self goToOrder];
        }
        else {
            [self goToWeb];
        }
    } failure:^{
        [self goToLogin];
    }];
}

- (void)goToWeb
{
    InitialViewController* viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    if ([viewController respondsToSelector:@selector(navToWeb)] == YES) {
        [viewController navToWeb];
    }
}

- (void)goToLogin
{
    InitialViewController* viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    if ([viewController respondsToSelector:@selector(navToLogin)] == YES) {
        [viewController navToLogin];
    }
}

- (void)goToOrder
{
    InitialViewController* viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    if ([viewController respondsToSelector:@selector(navToOrder)] == YES) {
        [viewController navToOrder];
    }
}


@end
