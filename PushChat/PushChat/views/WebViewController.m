//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  WebViewController.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/06
//

#import "WebViewController.h"

@interface WebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderNotificationReceived) name:@"newOrder" object:nil];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderNotificationReceived) name:@"newOrder" object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://teraflops-codejam.cloudapp.net:8090" ]]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)orderNotificationReceived {
    [self performSegueWithIdentifier:@"gotoOrderSegue" sender:self];
}

@end
