//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  BMViewController.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "BMViewController.h"

@interface BMViewController ()

@end

@implementation BMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    
    [self.view insertSubview:self.backgroundView atIndex:0 ];
    
    NSLayoutConstraint* bottomConstraint = [NSLayoutConstraint
                                            constraintWithItem:self.view
                                            attribute:NSLayoutAttributeBottom
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:self.backgroundView
                                            attribute:NSLayoutAttributeBottom
                                            multiplier:1.0
                                            constant:0];
    NSLayoutConstraint* topConstraint = [NSLayoutConstraint
                                            constraintWithItem:self.view
                                            attribute:NSLayoutAttributeTop
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:self.backgroundView
                                            attribute:NSLayoutAttributeBottom
                                            multiplier:1.0
                                            constant:0];
    NSLayoutConstraint* leftConstraint = [NSLayoutConstraint
                                            constraintWithItem:self.view
                                            attribute:NSLayoutAttributeLeft
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:self.backgroundView
                                            attribute:NSLayoutAttributeBottom
                                            multiplier:1.0
                                            constant:0];
    NSLayoutConstraint* rightConstraint = [NSLayoutConstraint
                                            constraintWithItem:self.view
                                            attribute:NSLayoutAttributeRight
                                            relatedBy:NSLayoutRelationEqual
                                            toItem:self.backgroundView
                                            attribute:NSLayoutAttributeBottom
                                            multiplier:1.0
                                            constant:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
