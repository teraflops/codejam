//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  InitialViewController.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/05
//

#import "InitialViewController.h"

@interface InitialViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightContraint;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end

@implementation InitialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navToLogin
{
    [self.view layoutIfNeeded];
    self.logoHeightContraint.constant = (self.view.frame.size.height/2) - 150;
    
    [UIView animateWithDuration:1.0f animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    }];
}

- (void)navToOrder
{
    [self performSegueWithIdentifier:@"orderSegue" sender:self];
}

- (void)navToWeb
{
    [self performSegueWithIdentifier:@"webSegue" sender:self];
}

@end
