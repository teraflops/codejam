//-----------------------------------------------------------------------------
//                                                                             |
//  Copyright The Teraflops Group Inc. 2014                                     |
//                                                                             |
//  All Rights Reserved                                                        |
//                                                                             |
//  Notice: All information contained herein is and remains the property       |
//          of the Teraflops Group Inc. The intellectual and technical          |
//          concepts contained herein are Proprietary to the Teraflops Group    |
//          Inc. and may be covered by patents and patents in process and      |
//          are protected by trade secret and copyright laws. Dissemination    |
//          of this information or reproduction of this material (including    |
//          Source Code) is strictly forbidden unless prior written consent    |
//          is obtained from the Teraflops Group Inc.                           |
//                                                                             |
//-----------------------------------------------------------------------------
//
//  OrderViewController.m
//  PushChat
//
//  Created by Chris Kieser on 2014/12/06
//

#import "OrderViewController.h"
#import "BMApiService.h"

@interface OrderViewController ()
@property (weak, nonatomic) IBOutlet UILabel *OrderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *etaLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *etaTitleLabel;

@property (strong, nonatomic, readwrite) NSTimer* timer;

@end

@implementation OrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self update];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(update) userInfo:nil repeats:YES];
}

- (void)update
{
    [BMApiService getOrderWithStatus:@"COLLECTED" invert:YES success:^(NSDictionary *response) {
        
        if ((response == nil) || ([response count] == 0)) {
            [self setStatus:@"COLLECTED"];
        }
        else {
            NSMutableDictionary* array = [response mutableCopy];
            
            NSString* state = array[@"state"];
            long orderNumber = [array[@"id"] longValue];
            long etaMillis = [array[@"eta"] longValue];
            
            NSDate* eta = [NSDate dateWithTimeIntervalSince1970:(etaMillis/1000)];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
            [dateFormatter setDoesRelativeDateFormatting:YES];
            
            NSString* friendlyDate = [dateFormatter stringFromDate:eta];
            self.OrderNumberLabel.text = [NSString stringWithFormat:@"#%ld",orderNumber];
            self.etaLabel.text = friendlyDate;
            [self setStatus:state];
        }
    } failure:^{
    }];
}

- (void)dealloc
{
    [self.timer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setStatus:(NSString*)statusString
{
    if ([statusString  isEqual: @"INQUEUE"]) {
        [self showHeadings:NO];
        self.statusLabel.text = @"In the queue";
    }
    else if ([statusString  isEqual: @"PREPARING"]) {
        [self showHeadings:NO];
        self.statusLabel.text = @"Preparing";
    }
    else if ([statusString  isEqual: @"READY"]) {
        [self showHeadings:NO];
        self.statusLabel.text = @"Collection";
    }
    // COLLECTED
    else {
        [self showHeadings:YES];
        self.statusLabel.text = @"No orders";
    }
}

- (void)showHeadings:(BOOL)showHeadings {
    self.etaTitleLabel.hidden = showHeadings;
    self.orderTitleLabel.hidden = showHeadings;
    self.OrderNumberLabel.hidden = showHeadings;
    self.etaLabel.hidden = showHeadings;
}

@end
