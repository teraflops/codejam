package cib.jas.web.rest;

import cib.jas.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import cib.jas.Application;
import cib.jas.domain.OrderItem;
import cib.jas.repository.OrderItemRepository;

import static cib.jas.util.TestUtils.inReverse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OrderItemResource REST controller.
 *
 * @see OrderItemResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class OrderItemResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";
    
    private static final String DEFAULT_DESCRIPTION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPTION = "UPDATED_TEXT";
    
    private static final Integer DEFAULT_QUANTITY = 0;
    private static final Integer UPDATED_QUANTITY = 1;
    
    private static final String DEFAULT_CATEGORY = "SAMPLE_TEXT";
    private static final String UPDATED_CATEGORY = "UPDATED_TEXT";
    
    private static final BigDecimal DEFAULT_COST = BigDecimal.ZERO;
    private static final BigDecimal UPDATED_COST = BigDecimal.ONE;
    

    @Inject
    private OrderItemRepository orderItemRepository;

    private MockMvc restOrderItemMockMvc;

    private OrderItem orderItem;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OrderItemResource orderItemResource = new OrderItemResource();
        ReflectionTestUtils.setField(orderItemResource, "orderItemRepository", orderItemRepository);
        this.restOrderItemMockMvc = MockMvcBuilders.standaloneSetup(orderItemResource).build();
    }

    @Before
    public void initTest() {
        orderItem = new OrderItem();
        orderItem.setName(DEFAULT_NAME);
        orderItem.setDescription(DEFAULT_DESCRIPTION);
        orderItem.setQuantity(DEFAULT_QUANTITY);
        orderItem.setCategory(DEFAULT_CATEGORY);
        orderItem.setCost(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void createOrderItem() throws Exception {
        // Validate the database is empty
        assertThat(orderItemRepository.findAll()).hasSize(96);

        // Create the OrderItem
        restOrderItemMockMvc.perform(post("/app/rest/orderItems")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderItem)))
                .andExpect(status().isOk());

        // Validate the OrderItem in the database
        List<OrderItem> orderItems = orderItemRepository.findAll();
        assertThat(orderItems).hasSize(97);
        OrderItem testOrderItem = orderItems.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testOrderItem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testOrderItem.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testOrderItem.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testOrderItem.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testOrderItem.getCost()).isEqualTo(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void getAllOrderItems() throws Exception {
        // Initialize the database
        orderItemRepository.saveAndFlush(orderItem);

        // Get all the orderItems
        restOrderItemMockMvc.perform(get("/app/rest/orderItems"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[96].id").value(orderItem.getId().intValue()))
                .andExpect(jsonPath("$.[96].name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.[96].description").value(DEFAULT_DESCRIPTION.toString()))
                .andExpect(jsonPath("$.[96].quantity").value(DEFAULT_QUANTITY))
                .andExpect(jsonPath("$.[96].category").value(DEFAULT_CATEGORY.toString()))
                .andExpect(jsonPath("$.[96].cost").value(DEFAULT_COST.intValue()));
    }

    @Test
    @Transactional
    public void getOrderItem() throws Exception {
        // Initialize the database
        orderItemRepository.saveAndFlush(orderItem);

        // Get the orderItem
        restOrderItemMockMvc.perform(get("/app/rest/orderItems/{id}", orderItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(orderItem.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOrderItem() throws Exception {
        // Get the orderItem
        restOrderItemMockMvc.perform(get("/app/rest/orderItems/{id}", 999L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderItem() throws Exception {
        // Initialize the database
        orderItemRepository.saveAndFlush(orderItem);

        // Update the orderItem
        orderItem.setName(UPDATED_NAME);
        orderItem.setDescription(UPDATED_DESCRIPTION);
        orderItem.setQuantity(UPDATED_QUANTITY);
        orderItem.setCategory(UPDATED_CATEGORY);
        orderItem.setCost(UPDATED_COST);
        restOrderItemMockMvc.perform(post("/app/rest/orderItems")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderItem)))
                .andExpect(status().isOk());

        // Validate the OrderItem in the database
        List<OrderItem> orderItems = orderItemRepository.findAll();
        assertThat(orderItems).hasSize(97);
        OrderItem testOrderItem = orderItems.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testOrderItem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testOrderItem.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testOrderItem.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testOrderItem.getCategory()).isEqualTo(UPDATED_CATEGORY);
        assertThat(testOrderItem.getCost()).isEqualTo(UPDATED_COST);
    }

    @Test
    @Transactional
    public void deleteOrderItem() throws Exception {
        // Initialize the database
        orderItemRepository.saveAndFlush(orderItem);

        // Get the orderItem
        restOrderItemMockMvc.perform(delete("/app/rest/orderItems/{id}", orderItem.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<OrderItem> orderItems = orderItemRepository.findAll();
        assertThat(orderItems).hasSize(96);
    }
}
