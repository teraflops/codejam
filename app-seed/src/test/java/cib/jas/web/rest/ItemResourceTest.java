package cib.jas.web.rest;

import cib.jas.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

import cib.jas.Application;
import cib.jas.domain.Item;
import cib.jas.repository.ItemRepository;

import static cib.jas.util.TestUtils.inReverse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ItemResource REST controller.
 *
 * @see ItemResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ItemResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";
    
    private static final String DEFAULT_DESCRIPTION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPTION = "UPDATED_TEXT";
    
    private static final Integer DEFAULT_QUANTITY = 0;
    private static final Integer UPDATED_QUANTITY = 1;
    
    private static final Item.Category DEFAULT_CATEGORY = Item.Category.COFFEE;
    private static final Item.Category UPDATED_CATEGORY = Item.Category.FOOD;
    
    private static final BigDecimal DEFAULT_COST = BigDecimal.ZERO;
    private static final BigDecimal UPDATED_COST = BigDecimal.ONE;
    

    @Inject
    private ItemRepository itemRepository;

    private MockMvc restItemMockMvc;

    private Item item;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ItemResource itemResource = new ItemResource();
        ReflectionTestUtils.setField(itemResource, "itemRepository", itemRepository);
        this.restItemMockMvc = MockMvcBuilders.standaloneSetup(itemResource).build();
    }

    @Before
    public void initTest() {
        item = new Item();
        item.setName(DEFAULT_NAME);
        item.setDescription(DEFAULT_DESCRIPTION);
        item.setQuantity(DEFAULT_QUANTITY);
        item.setCategory(DEFAULT_CATEGORY);
        item.setCost(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void createItem() throws Exception {
        // Validate the database is empty
        assertThat(itemRepository.findAll()).hasSize(68);

        // Create the Item
        restItemMockMvc.perform(post("/app/rest/items")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(item)))
                .andExpect(status().isOk());

        // Validate the Item in the database
        List<Item> items = itemRepository.findAll();
        assertThat(items).hasSize(69);
        Item testItem = items.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testItem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testItem.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testItem.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testItem.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testItem.getCost()).isEqualTo(DEFAULT_COST);
    }

    @Test
    @Transactional
    public void getAllItems() throws Exception {
        // Initialize the database
        itemRepository.saveAndFlush(item);

        // Get all the items
        restItemMockMvc.perform(get("/app/rest/items"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[68].id").value(item.getId().intValue()))
                .andExpect(jsonPath("$.[68].name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.[68].description").value(DEFAULT_DESCRIPTION.toString()))
                .andExpect(jsonPath("$.[68].quantity").value(DEFAULT_QUANTITY))
                .andExpect(jsonPath("$.[68].category").value(DEFAULT_CATEGORY.toString()))
                .andExpect(jsonPath("$.[68].cost").value(DEFAULT_COST.intValue()));
    }

    @Test
    @Transactional
    public void getItem() throws Exception {
        // Initialize the database
        itemRepository.saveAndFlush(item);

        // Get the item
        restItemMockMvc.perform(get("/app/rest/items/{id}", item.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(item.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingItem() throws Exception {
        // Get the item
        restItemMockMvc.perform(get("/app/rest/items/{id}", 999L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateItem() throws Exception {
        // Initialize the database
        itemRepository.saveAndFlush(item);

        // Update the item
        item.setName(UPDATED_NAME);
        item.setDescription(UPDATED_DESCRIPTION);
        item.setQuantity(UPDATED_QUANTITY);
        item.setCategory(UPDATED_CATEGORY);
        item.setCost(UPDATED_COST);
        restItemMockMvc.perform(post("/app/rest/items")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(item)))
                .andExpect(status().isOk());

        // Validate the Item in the database
        List<Item> items = itemRepository.findAll();
        assertThat(items).hasSize(69);
        Item testItem = items.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testItem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testItem.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testItem.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testItem.getCategory()).isEqualTo(UPDATED_CATEGORY);
        assertThat(testItem.getCost()).isEqualTo(UPDATED_COST);
    }

    @Test
    @Transactional
    public void deleteItem() throws Exception {
        // Initialize the database
        itemRepository.saveAndFlush(item);

        // Get the item
        restItemMockMvc.perform(delete("/app/rest/items/{id}", item.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Item> items = itemRepository.findAll();
        assertThat(items).hasSize(68);
    }
}
