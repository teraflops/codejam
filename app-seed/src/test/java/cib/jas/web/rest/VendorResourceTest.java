package cib.jas.web.rest;

import cib.jas.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import cib.jas.Application;
import cib.jas.domain.Vendor;
import cib.jas.repository.VendorRepository;

import static cib.jas.util.TestUtils.inReverse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VendorResource REST controller.
 *
 * @see VendorResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class VendorResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";
    
    private static final String DEFAULT_LOCATION = "SAMPLE_TEXT";
    private static final String UPDATED_LOCATION = "UPDATED_TEXT";
    
    private static final String DEFAULT_IBEACON = "SAMPLE_TEXT";
    private static final String UPDATED_IBEACON = "UPDATED_TEXT";
    

    @Inject
    private VendorRepository vendorRepository;

    private MockMvc restVendorMockMvc;

    private Vendor vendor;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VendorResource vendorResource = new VendorResource();
        ReflectionTestUtils.setField(vendorResource, "vendorRepository", vendorRepository);
        this.restVendorMockMvc = MockMvcBuilders.standaloneSetup(vendorResource).build();
    }

    @Before
    public void initTest() {
        vendor = new Vendor();
        vendor.setName(DEFAULT_NAME);
        vendor.setLocation(DEFAULT_LOCATION);
        vendor.setIbeacon(DEFAULT_IBEACON);
    }

    @Test
    @Transactional
    public void createVendor() throws Exception {
        // Validate the database is empty
        assertThat(vendorRepository.findAll()).hasSize(5);

        // Create the Vendor
        restVendorMockMvc.perform(post("/app/rest/vendors")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vendor)))
                .andExpect(status().isOk());

        // Validate the Vendor in the database
        List<Vendor> vendors = vendorRepository.findAll();
        assertThat(vendors).hasSize(6);
        Vendor testVendor = vendors.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testVendor.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVendor.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testVendor.getIbeacon()).isEqualTo(DEFAULT_IBEACON);
    }

    @Test
    @Transactional
    public void getAllVendors() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

        // Get all the vendors
        restVendorMockMvc.perform(get("/app/rest/vendors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[5].id").value(vendor.getId().intValue()))
                .andExpect(jsonPath("$.[5].name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.[5].location").value(DEFAULT_LOCATION.toString()))
                .andExpect(jsonPath("$.[5].ibeacon").value(DEFAULT_IBEACON.toString()));
    }

    @Test
    @Transactional
    public void getVendor() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

        // Get the vendor
        restVendorMockMvc.perform(get("/app/rest/vendors/{id}", vendor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(vendor.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.ibeacon").value(DEFAULT_IBEACON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVendor() throws Exception {
        // Get the vendor
        restVendorMockMvc.perform(get("/app/rest/vendors/{id}", 999L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVendor() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

        // Update the vendor
        vendor.setName(UPDATED_NAME);
        vendor.setLocation(UPDATED_LOCATION);
        vendor.setIbeacon(UPDATED_IBEACON);
        restVendorMockMvc.perform(post("/app/rest/vendors")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vendor)))
                .andExpect(status().isOk());

        // Validate the Vendor in the database
        List<Vendor> vendors = vendorRepository.findAll();
        assertThat(vendors).hasSize(6);
        Vendor testVendor = vendors.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testVendor.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVendor.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testVendor.getIbeacon()).isEqualTo(UPDATED_IBEACON);
    }

    @Test
    @Transactional
    public void deleteVendor() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

        // Get the vendor
        restVendorMockMvc.perform(delete("/app/rest/vendors/{id}", vendor.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Vendor> vendors = vendorRepository.findAll();
        assertThat(vendors).hasSize(5);
    }
}
