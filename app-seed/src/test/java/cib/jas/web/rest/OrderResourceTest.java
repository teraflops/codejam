package cib.jas.web.rest;

import cib.jas.Application;
import cib.jas.domain.Order;
import cib.jas.domain.Order.State;
import cib.jas.domain.User;
import cib.jas.repository.OrderRepository;
import cib.jas.repository.UserRepository;
import cib.jas.repository.VendorRepository;
import cib.jas.service.apns.ApnsMessages;
import cib.jas.service.apns.ApnsNotificationService;
import cib.jas.service.apns.TestApnsNotificationProvider;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static cib.jas.domain.Order.State.COLLECTED;
import static cib.jas.domain.Order.State.INQUEUE;
import static cib.jas.domain.Order.State.PREPARING;
import static cib.jas.domain.Order.State.READY;
import static cib.jas.util.TestUtils.inReverse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the OrderResource REST controller.
 *
 * @see OrderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class OrderResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

    private static final State DEFAULT_STATE = INQUEUE;
    private static final State UPDATED_STATE = State.PREPARING;

    private static final DateTime DEFAULT_ORDERED = new DateTime(0L);
    private static final DateTime UPDATED_ORDERED = new DateTime().withMillisOfSecond(0);
    private static final String DEFAULT_ORDERED_STR = dateTimeFormatter.print(DEFAULT_ORDERED);

    @Inject
    private UserRepository userRepository;

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private VendorRepository vendorRepository;

    @Inject
    private ApnsMessages apnsMessages;

    @Inject
    private ApnsNotificationService apnsNotificationService;

    private TestApnsNotificationProvider apnsNotificationProvider;

    private MockMvc restOrderMockMvc;

    private Order order;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OrderResource orderResource = new OrderResource();
        apnsNotificationProvider = new TestApnsNotificationProvider();
        ReflectionTestUtils.setField(orderResource, "orderRepository", orderRepository);
        ReflectionTestUtils.setField(orderResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(orderResource, "vendorRepository", vendorRepository);
        ReflectionTestUtils.setField(apnsNotificationService, "apnsNotificationProvider", apnsNotificationProvider);
        ReflectionTestUtils.setField(orderResource, "apnsMessages", apnsMessages);
        ReflectionTestUtils.setField(orderResource, "apnsNotificationService", apnsNotificationService);
        this.restOrderMockMvc = MockMvcBuilders.standaloneSetup(orderResource).build();
    }

    @Before
    public void initTest() {
        order = new Order();
        order.setState(DEFAULT_STATE);
        order.setOrdered(DEFAULT_ORDERED);
    }

    @Test
    @Transactional
    public void createOrder() throws Exception {
        // Validate the database is empty
        assertThat(orderRepository.findAll()).hasSize(48);

        order.setVendor(vendorRepository.findOne(1L));
        order.setUser(userRepository.findOne("customer1"));

        // Create the Order
        restOrderMockMvc.perform(post("/app/rest/orders")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(TestUtil.convertObjectToJsonBytes(order)))
                .andExpect(status().isOk());

        // Validate the Order in the database
        List<Order> orders = orderRepository.findAll();
        assertThat(orders).hasSize(49);
        Order testOrder = orders.stream().collect(inReverse()).stream().findFirst().get();
        assertThat(testOrder.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testOrder.getOrdered()).isEqualTo(DEFAULT_ORDERED);
    }

    @Test
    @Transactional
    public void getAllOrders() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get all the orders
        restOrderMockMvc.perform(get("/app/rest/orders"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[48].id").value(order.getId().intValue()))
                .andExpect(jsonPath("$.[48].state").value(DEFAULT_STATE.toString()))
                .andExpect(jsonPath("$.[48].ordered").value(DEFAULT_ORDERED_STR));
    }

    @Test
    @Transactional
    public void getOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get the order
        restOrderMockMvc.perform(get("/app/rest/orders/{id}", order.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(order.getId().intValue()))
                .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
                .andExpect(jsonPath("$.ordered").value(DEFAULT_ORDERED_STR));
    }

    @Test
    @Transactional
    public void getNonExistingOrder() throws Exception {
        // Get the order
        restOrderMockMvc.perform(get("/app/rest/orders/{id}", 999L))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get the order
        restOrderMockMvc.perform(delete("/app/rest/orders/{id}", order.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Order> orders = orderRepository.findAll();
        assertThat(orders).hasSize(48);
    }

    @Test
    @Transactional
    public void testMoveOrderToPreparingState() throws Exception {
        // Initialize the database
        User user = new User();
        user.setLogin("inqueue-state-user");
        user.setDeviceToken("123-456-789");
        user.setCreatedBy("TEST");
        order.setUser(userRepository.saveAndFlush(user));
        orderRepository.saveAndFlush(order);

        restOrderMockMvc.perform(put("/app/rest/orders/{id}/preparing", order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.state").value(PREPARING.name()));

        assertThat(apnsNotificationProvider.getMessage()).isEqualTo(wrapApns(apnsMessages.getPreparingMessage(order), order.getId().toString()));

        orderRepository.delete(order.getId());
    }

    @Test
    @Transactional
    public void testMoveOrderToReadyState() throws Exception {
        // Initialize the database
        User user = new User();
        user.setLogin("inqueue-state-user");
        user.setDeviceToken("123-456-789");
        user.setCreatedBy("TEST");
        order.setUser(userRepository.saveAndFlush(user));
        orderRepository.saveAndFlush(order);

        restOrderMockMvc.perform(put("/app/rest/orders/{id}/ready", order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.state").value(READY.name()));

        assertThat(apnsNotificationProvider.getMessage()).isEqualTo(wrapApns(apnsMessages.getReadyMessage(order), order.getId().toString()));

        orderRepository.delete(order.getId());
    }

    @Test
    @Transactional
    public void testMoveOrderToCollectedState() throws Exception {
        // Initialize the database
        User user = new User();
        user.setLogin("inqueue-state-user");
        user.setDeviceToken("123-456-789");
        user.setCreatedBy("TEST");
        order.setUser(userRepository.saveAndFlush(user));
        orderRepository.saveAndFlush(order);

        restOrderMockMvc.perform(put("/app/rest/orders/{id}/collected", order.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.state").value(COLLECTED.name()));

        orderRepository.delete(order.getId());
    }

    private String wrapApns(final String payload, final String orderId) {
        return "{\"orderId\":" + orderId + ",\"aps\":{\"alert\":\"" + payload + "\",\"sound\":\"default\"}}";
    }
}
