package cib.jas.domain;

import cib.jas.repository.OrderRepository;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class OrderTest {
    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private Order order;

    @Test
    public void testSetOrderEta() {
        DateTime now = DateTime.now();

        Order order1 = new Order();
        order1.setOrdered(now.minusMinutes(10));
        order1.setCompleted(now.minusMinutes(4));
        //6

        Order order2 = new Order();
        order2.setOrdered(now.minusMinutes(8));
        order2.setCompleted(now.minusMinutes(3));
        //5

        Order order3 = new Order();
        order3.setOrdered(now.minusMinutes(5));
        order3.setCompleted(now.minusMinutes(1));
        //4

        order.setOrdered(now);

        Mockito.when(orderRepository.findByVendorAndCompletedAfter(any(Vendor.class), any(DateTime.class))).thenReturn(
                ImmutableList.of(order1, order2, order3));

        order.setOrderEta(orderRepository);
        assertThat(order.getEta()).isEqualTo(now.plusMinutes(5));
    }
}