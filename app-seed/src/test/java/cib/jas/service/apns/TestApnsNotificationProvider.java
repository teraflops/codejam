package cib.jas.service.apns;

import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.EnhancedApnsNotification;
import com.notnoop.exceptions.NetworkIOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class TestApnsNotificationProvider implements ApnsNotificationProvider {

    private static final Logger log = LoggerFactory.getLogger(TestApnsNotificationProvider.class);

    private String message;

    @Override
    public ApnsService service() {
        return new ApnsService() {

            @Override
            public ApnsNotification push(final String deviceToken, final String payload) throws NetworkIOException {
                log.debug("Pushing message for token [{}]: {}", deviceToken, payload);
                message = payload;

                return new ApnsNotification() {

                    @Override
                    public byte[] getDeviceToken() {
                        return deviceToken.getBytes();
                    }

                    @Override
                    public byte[] getPayload() {
                        return message.getBytes();
                    }

                    @Override
                    public int getIdentifier() {
                        return (int) (Math.random() * 100);
                    }

                    @Override
                    public int getExpiry() {
                        return 0;
                    }

                    @Override
                    public byte[] marshall() {
                        return new byte[0];
                    }
                };
            }

            @Override
            public EnhancedApnsNotification push(final String deviceToken, final String payload, final Date expiry) throws NetworkIOException {
                return null;
            }

            @Override
            public ApnsNotification push(final byte[] deviceToken, final byte[] payload) throws NetworkIOException {
                return null;
            }

            @Override
            public EnhancedApnsNotification push(final byte[] deviceToken, final byte[] payload, final int expiry) throws NetworkIOException {
                return null;
            }

            @Override
            public Collection<? extends ApnsNotification> push(final Collection<String> deviceTokens, final String payload) throws NetworkIOException {
                return null;
            }

            @Override
            public Collection<? extends EnhancedApnsNotification> push(final Collection<String> deviceTokens, final String payload, final Date expiry) throws NetworkIOException {
                return null;
            }

            @Override
            public Collection<? extends ApnsNotification> push(final Collection<byte[]> deviceTokens, final byte[] payload) throws NetworkIOException {
                return null;
            }

            @Override
            public Collection<? extends EnhancedApnsNotification> push(final Collection<byte[]> deviceTokens, final byte[] payload, final int expiry) throws NetworkIOException {
                return null;
            }

            @Override
            public void push(final ApnsNotification message) throws NetworkIOException {

            }

            @Override
            public void start() {

            }

            @Override
            public void stop() {

            }

            @Override
            public Map<String, Date> getInactiveDevices() throws NetworkIOException {
                return null;
            }

            @Override
            public void testConnection() throws NetworkIOException {

            }
        };
    }

    public String getMessage() {
        return message;
    }
}
