package cib.jas;

import cib.jas.config.Constants;
import cib.jas.repository.ItemRepository;
import cib.jas.repository.UserRepository;
import cib.jas.repository.VendorRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import static cib.jas.domain.Item.Category.COFFEE;
import static cib.jas.domain.Item.Category.FOOD;

@ComponentScan
@EnableAutoConfiguration(exclude = {MetricFilterAutoConfiguration.class, MetricRepositoryAutoConfiguration.class})
public class Application implements ResourceLoaderAware {

    private final Logger log = LoggerFactory.getLogger(Application.class);

    @Inject
    private Environment env;

    @Inject
    private VendorRepository vendorRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ItemRepository itemRepository;

    private ResourceLoader resourceLoader;

    @Override
    public void setResourceLoader(final ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    /**
     * Initializes app_seed.
     * <p>
     * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
     * <p>
     */
    @PostConstruct
    public void initApplication() throws IOException {
        if (env.getActiveProfiles().length == 0) {
            log.warn("No Spring profile configured, running with default configuration");
        } else {
            log.info("Running with Spring profile(s) : {}", Arrays.toString(env.getActiveProfiles()));
        }

        if (!env.acceptsProfiles(Constants.SPRING_PROFILE_TEST)) {
            addVendorImages();
            addItemImages();
            addUserDeviceTokens();
        }
    }

    private void addUserDeviceTokens() {
        userRepository.findAllWithAuthorities().forEach(user -> {
            if (StringUtils.isBlank(user.getDeviceToken())) {
                user.setDeviceToken("13bb3af8 aa325ecc 8510d287 577df78a 45177cfe 76f76da1 5b05f082 9afb3b9d");
                userRepository.save(user);
            }
        });
    }

    private void addVendorImages() {
        vendorRepository.findVendorByName("Daily Buzz").forEach(vendor -> {
            if (vendor.getImage() == null) {
                try {
                    Resource resource = resourceLoader.getResource("classpath:config/images/vendors/buzz-button.png");
                    vendor.setImage(readImage(resource));
                    vendorRepository.save(vendor);
                    log.debug("Added image for vendor [{}]: {}", vendor.getName(), resource.getFilename());
                } catch (IOException e) {
                    log.error("Could not add default vendor image", e);
                }
            }
        });

        vendorRepository.findVendorByName("Oh So Heavenly").forEach(vendor -> {
            if (vendor.getImage() == null) {
                try {
                    Resource resource = resourceLoader.getResource("classpath:config/images/vendors/ohso-button.png");
                    vendor.setImage(readImage(resource));
                    vendorRepository.save(vendor);
                    log.debug("Added image for vendor [{}]: {}", vendor.getName(), resource.getFilename());
                } catch (IOException e) {
                    log.error("Could not add default vendor image", e);
                }
            }
        });

        vendorRepository.findVendorByName("Vida e Caffè").forEach(vendor -> {
            if (vendor.getImage() == null) {
                try {
                    Resource resource = resourceLoader.getResource("classpath:config/images/vendors/vida-button.png");
                    vendor.setImage(readImage(resource));
                    vendorRepository.save(vendor);
                    log.debug("Added image for vendor [{}]: {}", vendor.getName(), resource.getFilename());
                } catch (IOException e) {
                    log.error("Could not add default vendor image", e);
                }
            }
        });
    }

    private void addItemImages() {
        itemRepository.findByCategory(COFFEE).forEach(item -> {
            if (item.getImage() == null) {
                try {
                    Resource resource = resourceLoader.getResource("classpath:config/images/items/buzz-large.png");
                    item.setImage(readImage(resource));
                    itemRepository.save(item);
                    log.debug("Added image for item [{}]: {}", item.getName(), resource.getFilename());
                } catch (IOException e) {
                    log.error("Could not add default item image", e);
                }
            }
        });

        itemRepository.findByCategory(FOOD).forEach(item -> {
            if (item.getImage() == null) {
                try {
                    Resource resource = resourceLoader.getResource("classpath:config/images/items/sandwich.png");
                    item.setImage(readImage(resource));
                    itemRepository.save(item);
                    log.debug("Added image for item [{}]: {}", item.getName(), resource.getFilename());
                } catch (IOException e) {
                    log.error("Could not add default item image", e);
                }
            }
        });
    }

    /**
     * Main method, used to run the application.
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setShowBanner(false);

        SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);

        // Check if the selected profile has been set as argument.
        // if not the development profile will be added
        addDefaultProfile(app, source);
        addLiquibaseScanPackages();
        app.run(args);
    }

    /**
     * Set a default profile if it has not been set
     */
    private static void addDefaultProfile(SpringApplication app, SimpleCommandLinePropertySource source) {
        if (!source.containsProperty("spring.profiles.active")) {
            app.setAdditionalProfiles(Constants.SPRING_PROFILE_DEVELOPMENT);
        }
    }

    /**
     * Set the liquibases.scan.packages to avoid an exception from ServiceLocator.
     */
    private static void addLiquibaseScanPackages() {
        System.setProperty("liquibase.scan.packages", "liquibase.change" + "," + "liquibase.database" + "," +
                "liquibase.parser" + "," + "liquibase.precondition" + "," + "liquibase.datatype" + "," +
                "liquibase.serializer" + "," + "liquibase.sqlgenerator" + "," + "liquibase.executor" + "," +
                "liquibase.snapshot" + "," + "liquibase.logging" + "," + "liquibase.diff" + "," +
                "liquibase.structure" + "," + "liquibase.structurecompare" + "," + "liquibase.lockservice" + "," +
                "liquibase.ext" + "," + "liquibase.changelog");
    }

    private byte[] readImage(final Resource resource) throws IOException {
        BufferedImage originalImage;
        originalImage = ImageIO.read(resource.getInputStream());

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "png", byteArrayOutputStream);
        byteArrayOutputStream.flush();

        return byteArrayOutputStream.toByteArray();
    }
}
