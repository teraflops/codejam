package cib.jas.web.rest;

import cib.jas.domain.Item;
import cib.jas.domain.Order;
import cib.jas.domain.Order.State;
import cib.jas.domain.OrderItem;
import cib.jas.repository.ItemRepository;
import cib.jas.repository.OrderItemRepository;
import cib.jas.repository.OrderRepository;
import cib.jas.repository.UserRepository;
import cib.jas.repository.VendorRepository;
import cib.jas.service.apns.ApnsMessages;
import cib.jas.service.apns.ApnsNotificationService;
import com.codahale.metrics.annotation.Timed;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cib.jas.domain.Order.State.COLLECTED;
import static cib.jas.domain.Order.State.PREPARING;
import static cib.jas.domain.Order.State.READY;

/**
 * REST controller for managing Order.
 */
@RestController
@RequestMapping("/app")
public class OrderResource {

    private final Logger log = LoggerFactory.getLogger(OrderResource.class);

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private VendorRepository vendorRepository;

    @Inject
    private ApnsNotificationService apnsNotificationService;

    @Inject
    private ApnsMessages apnsMessages;

    @Inject
    private OrderItemRepository orderItemRepository;

    @Inject
    private ItemRepository itemRepository;

    /**
     * POST  /rest/orders -> Create a new order.
     */
    @RequestMapping(value = "/rest/orders",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public Order create(@RequestBody Order order) {
        log.debug("REST request to save Order : {}", order);

        List<OrderItem> persistedItems = new ArrayList<>(order.getOrderItems().size());
        for (OrderItem orderItem : order.getOrderItems()) {
            persistedItems.add(orderItemRepository.save(orderItem));
            Item relatedItem = itemRepository.findByNameAndVendor(orderItem.getName(), order.getVendor());
            relatedItem.setQuantity(relatedItem.getQuantity() - orderItem.getQuantity());
            itemRepository.save(relatedItem);
        }

        order.setOrderItems(persistedItems);

        order.setUser(userRepository.findOne(order.getUser().getLogin()));
        order.setVendor(vendorRepository.findOne(order.getVendor().getId()));
        orderRepository.save(order);

        persistedItems.forEach(orderItem -> {
            orderItem.setOrder(order);
            orderItemRepository.save(orderItem);
        });

        apnsNotificationService.sendNotification(order.getUser().getDeviceToken(), order, apnsMessages.getOrderCreatedMessage(order));

        return orderRepository.findOne(order.getId());
    }

    /**
     * GET  /rest/orders -> get all the orders.
     */
    @RequestMapping(value = "/rest/orders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Order> getAll() {
        log.debug("REST request to get all Orders");
        return orderRepository.findAll();
    }

    /**
     * GET  /rest/orders/:id -> get the "id" order.
     */
    @RequestMapping(value = "/rest/orders/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Order> get(@PathVariable Long id) {
        log.debug("REST request to get Order : {}", id);
        return Optional.ofNullable(orderRepository.findOne(id))
                .map(order -> new ResponseEntity<>(
                        order,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rest/orders/:id -> delete the "id" order.
     */
    @RequestMapping(value = "/rest/orders/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Order : {}", id);
        orderRepository.delete(id);
    }

    /**
     * PUT  /rest/orders/:id -> move the order state to State.PREPARING
     */
    @RequestMapping(value = "/rest/orders/{id}/preparing",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Order preparingState(@PathVariable Long id) {
        return transitionState(id, PREPARING);
    }

    /**
     * PUT  /rest/orders/:id -> move the order state to State.READY
     */
    @RequestMapping(value = "/rest/orders/{id}/ready",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Order readyState(@PathVariable Long id) {
        return transitionState(id, READY);
    }

    /**
     * PUT  /rest/orders/:id -> move the order state to State.COLLECTED
     */
    @RequestMapping(value = "/rest/orders/{id}/collected",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Order collectedState(@PathVariable Long id) {
        DateTime now = new DateTime();
        Order order = orderRepository.findOne(id);
        order.setCompleted(now);
        orderRepository.save(order);
        return transitionState(id, COLLECTED);
    }

    private Order transitionState(Long orderId, State state) {
        log.debug("REST request to move Order [{}] state to: {}", orderId, PREPARING);
        Order order = orderRepository.findOne(orderId);
        order.setState(state);

        order.setOrderEta(orderRepository);
        apnsMessages.getStateMessage(state, order).ifPresent(message -> apnsNotificationService.sendNotification(order.getUser().getDeviceToken(), order, message));

        Order orderEntity = orderRepository.save(order);

        order.setOrderEta(orderRepository);

        return orderEntity;
    }
}
