package cib.jas.web.rest;

import com.codahale.metrics.annotation.Timed;
import cib.jas.domain.OrderItem;
import cib.jas.repository.OrderItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OrderItem.
 */
@RestController
@RequestMapping("/app")
public class OrderItemResource {

    private final Logger log = LoggerFactory.getLogger(OrderItemResource.class);

    @Inject
    private OrderItemRepository orderItemRepository;

    /**
     * POST  /rest/orderItems -> Create a new orderItem.
     */
    @RequestMapping(value = "/rest/orderItems",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody OrderItem orderItem) {
        log.debug("REST request to save OrderItem : {}", orderItem);
        orderItemRepository.save(orderItem);
    }

    /**
     * GET  /rest/orderItems -> get all the orderItems.
     */
    @RequestMapping(value = "/rest/orderItems",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<OrderItem> getAll() {
        log.debug("REST request to get all OrderItems");
        return orderItemRepository.findAll();
    }

    /**
     * GET  /rest/orderItems/:id -> get the "id" orderItem.
     */
    @RequestMapping(value = "/rest/orderItems/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<OrderItem> get(@PathVariable Long id) {
        log.debug("REST request to get OrderItem : {}", id);
        return Optional.ofNullable(orderItemRepository.findOne(id))
            .map(orderItem -> new ResponseEntity<>(
                orderItem,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rest/orderItems/:id -> delete the "id" orderItem.
     */
    @RequestMapping(value = "/rest/orderItems/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete OrderItem : {}", id);
        orderItemRepository.delete(id);
    }
}
