package cib.jas.web.rest;

import cib.jas.domain.Order;
import cib.jas.domain.Vendor;
import cib.jas.repository.OrderRepository;
import cib.jas.repository.VendorRepository;
import com.codahale.metrics.annotation.Timed;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.QueryParam;
import java.util.*;

/**
 * REST controller for managing Vendor.
 */
@RestController
@RequestMapping("/app")
public class VendorResource {

    private final Logger log = LoggerFactory.getLogger(VendorResource.class);

    @Inject
    private VendorRepository vendorRepository;

    @Inject
    private OrderRepository orderRepository;

    /**
     * POST  /rest/vendors -> Create a new vendor.
     */
    @RequestMapping(value = "/rest/vendors",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Vendor vendor) {
        log.debug("REST request to save Vendor : {}", vendor);
        vendorRepository.save(vendor);
    }

    /**
     * GET  /rest/vendors -> get all the vendors.
     */
    @RequestMapping(value = "/rest/vendors",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Vendor> getAll() {
        log.debug("REST request to get all Vendors");
        return vendorRepository.findAll();
    }

    /**
     * GET  /rest/vendors -> get all the vendor names.
     */
    @RequestMapping(value = "/rest/vendorNames",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<String> getAllVendorNames() {
        log.debug("REST request to get all Vendors");

        //This is a lazy hack. Running out of time

        List<Vendor> all = getAll();
        Set<String> names = new HashSet<String>();
        for (Vendor vendor: all){
            names.add(vendor.getName());
        }

        List<String> namesReturn = new ArrayList<>();

        for (String name: names){
            namesReturn.add(name);
        }

        return namesReturn;
    }

    /**
     * GET  /rest/vendors/:id -> get the "id" vendor.
     */
    @RequestMapping(value = "/rest/vendors/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vendor> get(@PathVariable Long id) {
        log.debug("REST request to get Vendor : {}", id);
        return Optional.ofNullable(vendorRepository.findOne(id))
                .map(vendor -> new ResponseEntity<>(
                        vendor,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rest/vendors/:id -> delete the "id" vendor.
     */
    @RequestMapping(value = "/rest/vendors/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Vendor : {}", id);
        vendorRepository.delete(id);
    }

    /**
     * GET  /rest/vendors/:id/orders/:state -> get the orders in the "state" associated with "id" vendor.
     */
    @RequestMapping(value = "/rest/vendors/{id}/orders/{state}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Order> getOrdersInState(@PathVariable Long id, @PathVariable Order.State state) {
        log.debug("REST request to get Vendor order in a given state: {}", id, state);
        Vendor vendor = vendorRepository.findOne(id);
        List<Order> orders = orderRepository.findByStateAndVendor(state, vendor);
        orders.forEach(order ->
                        order.setOrderEta(orderRepository)
        );
        return orders;
    }

    /**
     * GET  /rest/vendors/:id/orders -> get the orders with "id" vendor.
     */
    @RequestMapping(value = "/rest/vendors/{id}/orders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Order> getOrders(@PathVariable Long id, @QueryParam("after") Long after, @QueryParam("completed") boolean completed) {
        log.debug("REST request to get Vendor order in a given state: {}", id, after);
        Vendor vendor = vendorRepository.findOne(id);

        DateTime date;
        if (after != null) {
            date = new DateTime(after);
        } else {
            date = new DateTime(0);
        }

        List<Order> orders;
        if(completed){
            orders = orderRepository.findByVendorAndCompletedAfter(vendor, date);
        }else if (!completed && after == null) {
            orders = orderRepository.findInProgressForVendor(vendor);
        }else{
            orders = orderRepository.findByVendorAndOrderedAfter(vendor, date);
        }
        orders.forEach(order ->
            order.setOrderEta(orderRepository)
        );

        return orders;
    }

    /**
     * GET  /rest/vendors/:id/orders -> count the orders placed after "after" associated with "id" vendor.
     */
    @RequestMapping(value = "/rest/vendors/{id}/orders/count",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Long getOrdersCount(@PathVariable Long id, @QueryParam("after") Long after) {
        log.debug("REST request to get Vendor order in a given state: {}", id, after);

        Vendor vendor = vendorRepository.findOne(id);
        DateTime date;
        if (after != null) {
            date = new DateTime(after);
        } else {
            date = new DateTime();
        }

        return orderRepository.countOrdersFromVendor(date, vendor);
    }

    /**
     * GET  /rest/vendors/:id/image -> get the "id" vendor's image.
     */
    @RequestMapping(value = "/rest/vendors/{id}/image",
            method = RequestMethod.GET,
            produces = MediaType.IMAGE_PNG_VALUE)
    @Timed
    public ResponseEntity<byte[]> getImage(@PathVariable Long id) {
        log.debug("REST request to get Vendor's image : {}", id);
        return Optional.ofNullable(vendorRepository.findOne(id))
                .map(vendor -> new ResponseEntity<>(
                        vendor.getImage(),
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * PUT  /rest/vendors/:id/image -> upload an image for "id" vendor.
     */
    @RequestMapping(value = "/rest/vendors/{id}/image",
            method = RequestMethod.PUT,
            consumes = MediaType.IMAGE_PNG_VALUE)
    @Timed
    public void uploadImage(@PathVariable Long id, HttpEntity<byte[]> image) {
        log.debug("REST request to upload Vendor's image : {}", id);
        Vendor vendor = vendorRepository.findOne(id);
        vendor.setImage(image.getBody());
        vendorRepository.save(vendor);
    }
}
