package cib.jas.web.rest;

import cib.jas.domain.Item;
import cib.jas.domain.Vendor;
import cib.jas.repository.ItemRepository;
import cib.jas.repository.VendorRepository;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Item.
 */
@RestController
@RequestMapping("/app")
public class ItemResource {

    private final Logger log = LoggerFactory.getLogger(ItemResource.class);

    @Inject
    private ItemRepository itemRepository;

    @Inject
    private VendorRepository vendorRepository;

    /**
     * POST  /rest/items -> Create a new item.
     */
    @RequestMapping(value = "/rest/items",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Item item) {
        log.debug("REST request to save Item : {}", item);
        itemRepository.save(item);
    }

    /**
     * GET  /rest/items -> get all the items.
     */
    @RequestMapping(value = "/rest/items",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Item> getAll(@QueryParam("vendor") Long vendor, @QueryParam("category") Item.Category category) {
        log.debug("REST request to get all Items");
        boolean vendorSearch = vendor != null;
        Vendor vendorEntity = null;

        if (vendorSearch) {
            vendorEntity = vendorRepository.findOne(vendor);
        }

        boolean categorySearch = category != null;
        /*if(categorySearch) {
            log.info(category.name());
        }*/

        if (categorySearch && vendorSearch) {
            return itemRepository.findByCategoryAndVendor(category, vendorEntity);
        } else if (categorySearch) {
            return itemRepository.findByCategory(category);
        } else if (vendorSearch) {
            return itemRepository.findByVendor(vendorEntity);
        } else {
            return itemRepository.findAll();
        }
    }

    /**
     * GET  /rest/items/:id -> get the "id" item.
     */
    @RequestMapping(value = "/rest/items/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Item> get(@PathVariable Long id) {
        log.debug("REST request to get Item : {}", id);
        return Optional.ofNullable(itemRepository.findOne(id))
                .map(item -> new ResponseEntity<>(
                        item,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rest/items/:id -> delete the "id" item.
     */
    @RequestMapping(value = "/rest/items/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Item : {}", id);
        itemRepository.delete(id);
    }

    /**
     * GET  /rest/items/:id/image -> get the "id" item's image.
     */
    @RequestMapping(value = "/rest/items/{id}/image",
            method = RequestMethod.GET,
            produces = MediaType.IMAGE_PNG_VALUE)
    @Timed
    public ResponseEntity<byte[]> getImage(@PathVariable Long id) {
        log.debug("REST request to get Image's image : {}", id);
        return Optional.ofNullable(itemRepository.findOne(id))
                .map(item -> new ResponseEntity<>(
                        item.getImage(),
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * PUT  /rest/items/:id/image -> upload an image for "id" item.
     */
    @RequestMapping(value = "/rest/items/{id}/image",
            method = RequestMethod.PUT,
            consumes = MediaType.IMAGE_PNG_VALUE)
    @Timed
    public void uploadImage(@PathVariable Long id, HttpEntity<byte[]> image) {
        log.debug("REST request to upload Image's image : {}", id);
        Item item = itemRepository.findOne(id);
        item.setImage(image.getBody());
        itemRepository.save(item);
    }

    /**
     * UPDATE  /rest/items/:id/setStock/:stock -> change quantity value to "stock"
     */
    @RequestMapping(value = "/rest/items/{id}/setStock/{stock}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void setItemQuantity(@PathVariable Long id, @PathVariable int stock) {
        log.debug("REST request to delete Item : {}", id);
        Item item = itemRepository.findOne(id);
        item.setQuantity(stock);
        itemRepository.save(item);
    }

}
