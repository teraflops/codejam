/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package cib.jas.web.rest.dto;
