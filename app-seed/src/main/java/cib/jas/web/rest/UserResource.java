package cib.jas.web.rest;

import cib.jas.domain.Order;
import cib.jas.domain.User;
import cib.jas.repository.OrderRepository;
import cib.jas.repository.UserRepository;
import cib.jas.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing users.
 */
@RestController
@RequestMapping("/app")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private OrderRepository orderRepository;

    /**
     * GET  /rest/users/:login -> get the "login" user.
     */
    @RequestMapping(value = "/rest/users/{login}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @RolesAllowed(AuthoritiesConstants.ADMIN)
    ResponseEntity<User> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return Optional.ofNullable(userRepository.findOneByLogin(login))
                .map(user -> new ResponseEntity<>(user, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /rest/users -> get all the users.
     */
    @RequestMapping(value = "/rest/users",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<User> getAll() {
        log.debug("REST request to get all User");
        return userRepository.findAll();
    }


    /**
     * GET  /rest/users/:login/orders/:state -> get the orders in the "state" associated with "login" user.
     */
    @RequestMapping(value = "/rest/users/{login}/orders/{state}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Order> getOrders(@PathVariable String login, @PathVariable Order.State state, @QueryParam("invert") boolean invert) {
        log.debug("REST request to get user orders : {}", login, state);
        User user = userRepository.findOneByLogin(login);
        if (!invert) {
            return orderRepository.findByStateAndUser(state, user);
        }
        else{
            return orderRepository.findByStateNotAndUser(state, user);
        }
    }


    /**
     * GET  /rest/users/:login/orders/:state -> get the orders in the "state" associated with "login" user.
     */
    @RequestMapping(value = "/rest/users/{login}/orders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Order> getAllOrders(@PathVariable String login) {
        log.debug("REST request to get user orders : {}", login);
        User user = userRepository.findOneByLogin(login);
        return orderRepository.findByUser(user);
    }


    /**
     * GET  /rest/users/:login/order/:state -> get the orders in the "state" associated with "login" user.
     */
    @RequestMapping(value = "/rest/users/{login}/order/{state}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Order getOrder(@PathVariable String login, @PathVariable Order.State state, @QueryParam("invert") boolean invert) {
        List<Order> orders = getOrders(login, state, invert);
        if (orders.size() > 0){
            return orders.get(0);
        }
        else{
            return null;
        }
    }



    /**
     * PUT  /rest/users/:login?deviceToken={deviceToken} -> add a Apple device token to this users profile
     */
    @RequestMapping(value = "/rest/users/{login}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public User addDeviceToken(@PathVariable String login, @RequestParam String deviceToken) {
        log.debug("REST request to add Apple device token: {}", deviceToken);
        User user = userRepository.findOneByLogin(login);
        user.setDeviceToken(deviceToken);

        return userRepository.save(user);
    }
}
