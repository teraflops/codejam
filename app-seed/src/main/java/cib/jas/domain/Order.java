package cib.jas.domain;

import cib.jas.domain.util.CustomDateTimeDeserializer;
import cib.jas.domain.util.CustomDateTimeSerializer;
import cib.jas.repository.OrderRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A Order.
 */
@Entity
@Table(name = "T_ORDER")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Order implements Serializable {
    public enum State {
        INQUEUE, PREPARING, READY, COLLECTED
    }

    private final static int DEFAULT_AVERAGE_DAY_SEARCH = 7;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private State state = State.INQUEUE;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "ordered", nullable = false)
    private DateTime ordered = new DateTime();

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "completed", nullable = false)
    private DateTime completed;

    @ManyToOne
    private Vendor vendor;

    @ManyToOne()
    private User user;

    @Transient
    @JsonProperty
    private DateTime eta;

    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<OrderItem> orderItems = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public DateTime getOrdered() {
        return ordered;
    }

    public void setOrdered(DateTime ordered) {
        this.ordered = ordered;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public DateTime getCompleted() {
        return completed;
    }

    public void setCompleted(DateTime completed) {
        this.completed = completed;
    }

    public DateTime getEta() {
        if (eta == null){
            eta = getOrdered().plusMinutes(6);
        }

        return eta;
    }

    public void setEta(DateTime eta) {
        this.eta = eta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Order order = (Order) o;

        if (id != null ? !id.equals(order.id) : order.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", state='" + state + "'" +
                ", ordered='" + ordered + "'" +
                '}';
    }

    public void setOrderEta(OrderRepository orderRepository){
        List<Order> completedOrders = orderRepository.findByVendorAndCompletedAfter(getVendor(), DateTime.now().minusDays(DEFAULT_AVERAGE_DAY_SEARCH));

        if (completedOrders.size() > 0) {
            long average = 0;
            for (Order completedOrder : completedOrders) {
                if (completedOrder.getCompleted()==null){
                    continue;
                }
                average += completedOrder.getCompleted().minus(completedOrder.getOrdered().getMillis()).getMillis();
            }

            average /= completedOrders.size();

            setEta(getOrdered().plus(average));

            orderRepository.save(this);
        }
    }
}
