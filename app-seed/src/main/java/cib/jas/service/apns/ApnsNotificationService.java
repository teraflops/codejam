package cib.jas.service.apns;

import cib.jas.domain.Order;
import cib.jas.service.apns.exception.ApnsException;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApnsNotificationService {

    @Autowired
    private ApnsNotificationProvider apnsNotificationProvider;

    public ApnsNotification sendNotification(String deviceToken, Order order, String message) throws ApnsException {
        ApnsService service = apnsNotificationProvider.service();

        String payload = APNS.newPayload()
                .alertBody(message)
                .sound("default")
                .customField("orderId", order.getId())
                .build();
        return service.push(deviceToken, payload);
    }
}
