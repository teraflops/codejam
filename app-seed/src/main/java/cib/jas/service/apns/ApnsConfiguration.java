package cib.jas.service.apns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ApnsConfiguration implements EnvironmentAware {

    private static final Logger log = LoggerFactory.getLogger(ApnsConfiguration.class);

    private RelaxedPropertyResolver propertyResolver;

    @Override
    public void setEnvironment(Environment environment) {
        this.propertyResolver = new RelaxedPropertyResolver(environment, "messages.");
    }

    @Bean
    public ApnsMessages apnsMessages() {
        log.debug("Configuring APNS messages...");
        ApnsMessages messages = new ApnsMessages();
        messages.setOrderCreatedMessage(propertyResolver.getProperty("orderCreated"));
        messages.setPreparingMessage(propertyResolver.getProperty("preparing"));
        messages.setReadyMessage(propertyResolver.getProperty("ready"));

        return messages;
    }
}
