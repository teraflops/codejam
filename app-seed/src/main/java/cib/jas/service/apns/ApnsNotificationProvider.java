package cib.jas.service.apns;

import com.notnoop.apns.ApnsService;

public interface ApnsNotificationProvider {

    ApnsService service();
}
