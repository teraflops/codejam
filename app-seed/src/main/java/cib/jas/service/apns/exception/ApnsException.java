package cib.jas.service.apns.exception;

public class ApnsException extends RuntimeException {

    public ApnsException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
