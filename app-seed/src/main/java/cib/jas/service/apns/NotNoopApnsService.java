package cib.jas.service.apns;

import cib.jas.service.apns.exception.ApnsException;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class NotNoopApnsService implements ApnsNotificationProvider, ResourceLoaderAware {

    private ResourceLoader resourceLoader;

    @Override
    public ApnsService service() {
        try {
            return APNS.newService()
                    .withCert(resourceLoader.getResource("classpath:brewmatter.p12").getInputStream(), "password")
                    .withSandboxDestination()
                    .build();
        } catch (IOException e) {
            throw new ApnsException("Could not locate brewmatter APNS private key", e);
        }
    }

    @Override
    public void setResourceLoader(final ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
