package cib.jas.service.apns;

import cib.jas.domain.Order;
import cib.jas.domain.Order.State;

import java.util.Optional;

import static java.lang.String.format;

public class ApnsMessages {

    private String orderCreatedMessage;
    private String preparingMessage;
    private String readyMessage;

    public Optional<String> getStateMessage(final State state, final Order order) {
        String message;
        switch (state) {
            case PREPARING: message = getPreparingMessage(order); break;
            case READY: message = getReadyMessage(order); break;
            default: message = null;
        }

        return Optional.ofNullable(message);
    }

    public String getPreparingMessage(Order order) {
        return format(preparingMessage, order.getId(), order.getEta().toString("HH:mm"));
    }

    public void setPreparingMessage(final String preparingMessage) {
        this.preparingMessage = preparingMessage;
    }

    public String getReadyMessage(Order order) {
        return format(readyMessage, order.getId(), order.getEta().toString("HH:mm"));
    }

    public void setReadyMessage(final String readyMessage) {
        this.readyMessage = readyMessage;
    }

    public String getOrderCreatedMessage(Order order) {
        return format(orderCreatedMessage, order.getId());
    }

    public void setOrderCreatedMessage(final String orderCreatedMessage) {
        this.orderCreatedMessage = orderCreatedMessage;
    }
}
