package cib.jas.repository;

import cib.jas.domain.Order;
import cib.jas.domain.User;
import cib.jas.domain.Vendor;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Order entity.
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    public List<Order> findByVendorAndCompletedAfter( Vendor vendor, DateTime completed);

    public List<Order> findByStateAndVendor(Order.State state, Vendor vendor);

    public List<Order> findByStateAndUser(Order.State state, User user);

    public List<Order> findByUser(User user);

    public List<Order> findByStateNotAndUser(Order.State state, User user);

    public List<Order> findByVendorAndOrderedAfter(Vendor vendor, DateTime ordered);

    @Query("SELECT COUNT(p) FROM Order p WHERE p.ordered >= :date AND p.vendor = :vendor")
    public Long countOrdersFromVendor(@Param("date") DateTime date, @Param("vendor") Vendor vendor);

    @Query("SELECT p FROM Order p WHERE p.state <> 'COLLECTED' AND p.vendor = :vendor ORDER BY p.id DESC")
    public List<Order> findInProgressForVendor(@Param("vendor") Vendor vendor);
}
