package cib.jas.repository;

import cib.jas.domain.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Vendor entity.
 */
public interface VendorRepository extends JpaRepository<Vendor, Long> {
    List<Vendor> findVendorByName(String name);
}
