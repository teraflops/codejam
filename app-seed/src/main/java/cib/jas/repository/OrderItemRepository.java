package cib.jas.repository;

import cib.jas.domain.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the OrderItem entity.
 */
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
