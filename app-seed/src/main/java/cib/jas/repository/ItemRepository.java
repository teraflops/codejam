package cib.jas.repository;

import cib.jas.domain.Item;
import cib.jas.domain.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Item entity.
 */
public interface ItemRepository extends JpaRepository<Item, Long> {
    List<Item> findByCategory(Item.Category category);
    List<Item> findByVendor(Vendor vendor);
    List<Item> findByCategoryAndVendor(Item.Category category, Vendor vendor);
    Item findByNameAndVendor(String name, Vendor vendor);
}
