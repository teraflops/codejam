'use strict';

appSeedApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/order', {
                    templateUrl: 'views/orders.html',
                    controller: 'OrderController',
                    resolve:{
                        resolvedOrder: ['Order', function (Order) {
                            return Order.query().$promise;
                        }],
                        resolvedVendor: ['Vendor', function (Vendor) {
                            return Vendor.query().$promise;
                        }],
                        resolvedUser: ['User', function (User) {
                            return User.query().$promise;
                        }],
                        resolvedOrderItem: ['OrderItem', function (OrderItem) {
                            return OrderItem.query().$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
