'use strict';

appSeedApp.factory('Order', function ($resource) {
        return $resource('app/rest/orders/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });

appSeedApp.factory('OrderUser', function ($resource) {
    return $resource('app/rest/users/:id/orders', {id: '@id'}, {
        'get': { method: 'GET', isArray: true}
    });
});
