'use strict';

appSeedApp.controller('OrderController', function ($scope, resolvedOrder, Order, resolvedVendor, resolvedUser, resolvedOrderItem) {

        $scope.orders = resolvedOrder;
        $scope.vendors = resolvedVendor;
        $scope.users = resolvedUser;
        $scope.orderItems = resolvedOrderItem;

        $scope.create = function () {
            Order.save($scope.order,
                function () {
                    $scope.orders = Order.query();
                    $('#saveOrderModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.order = Order.get({id: id});
            $('#saveOrderModal').modal('show');
        };

        $scope.delete = function (id) {
            Order.delete({id: id},
                function () {
                    $scope.orders = Order.query();
                });
        };

        $scope.clear = function () {
            $scope.order = {state: null, ordered: null, id: null};
        };
    });
