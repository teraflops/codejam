'use strict';

appSeedApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/item', {
                    templateUrl: 'views/items.html',
                    controller: 'ItemController',
                    resolve:{
                        resolvedItem: ['Item', function (Item) {
                            return Item.query().$promise;
                        }],
                        resolvedVendor: ['Vendor', function (Vendor) {
                            return Vendor.query().$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
