'use strict';

appSeedApp.controller('ItemController', function ($scope, resolvedItem, Item, resolvedVendor) {

        $scope.items = resolvedItem;
        $scope.vendors = resolvedVendor;

        $scope.create = function () {
            Item.save($scope.item,
                function () {
                    $scope.items = Item.query();
                    $('#saveItemModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.item = Item.get({id: id});
            $('#saveItemModal').modal('show');
        };

        $scope.delete = function (id) {
            Item.delete({id: id},
                function () {
                    $scope.items = Item.query();
                });
        };

        $scope.clear = function () {
            $scope.item = {name: null, description: null, quantity: null, category: null, cost: null, id: null};
        };
    });
