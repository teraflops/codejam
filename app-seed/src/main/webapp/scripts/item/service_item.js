'use strict';

appSeedApp.factory('Item', function ($resource) {
        return $resource('app/rest/items/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });

appSeedApp.factory('VendorItem', function ($resource) {
    return $resource('app/rest/items?vendor=:vendor&category=:category', {vendor: '@vendor', category: '@category'}, {
        'query': { method: 'GET', isArray: true}
    });
});