'use strict';

appSeedApp.controller('VendorController', function ($scope, resolvedVendor, Vendor, resolvedItem) {

        $scope.vendors = resolvedVendor;
        $scope.items = resolvedItem;

        $scope.create = function () {
            Vendor.save($scope.vendor,
                function () {
                    $scope.vendors = Vendor.query();
                    $('#saveVendorModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.vendor = Vendor.get({id: id});
            $('#saveVendorModal').modal('show');
        };

        $scope.delete = function (id) {
            Vendor.delete({id: id},
                function () {
                    $scope.vendors = Vendor.query();
                });
        };

        $scope.clear = function () {
            $scope.vendor = {name: null, location: null, ibeacon: null, id: null};
        };
    });
