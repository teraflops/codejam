'use strict';

appSeedApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/vendor', {
                    templateUrl: 'views/vendors.html',
                    controller: 'VendorController',
                    resolve:{
                        resolvedVendor: ['Vendor', function (Vendor) {
                            return Vendor.query().$promise;
                        }],
                        resolvedItem: ['Item', function (Item) {
                            return Item.query().$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
