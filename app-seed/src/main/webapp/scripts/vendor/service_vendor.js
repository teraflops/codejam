'use strict';

appSeedApp.factory('Vendor', function ($resource) {
        return $resource('app/rest/vendors/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });

appSeedApp.factory('VendorOrders', function ($resource) {
    return $resource('app/rest/vendors/:id/orders/:state', {id: '@id', state: '@state'}, {
        'query': { method: 'GET', isArray: true}
    });
});