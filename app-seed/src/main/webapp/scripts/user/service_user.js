'use strict';

appSeedApp.factory('User', function ($resource) {
        return $resource('app/rest/users/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
