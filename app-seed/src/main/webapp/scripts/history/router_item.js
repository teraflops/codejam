'use strict';

appSeedApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/history', {
                    templateUrl: 'views/history.html',
                    controller: 'HistoryController',
                    resolve:{
                        resolvedHistoryOrders: ['OrderUser', function (OrderUser) {
                            return OrderUser.get({id:'customer1'}).$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
