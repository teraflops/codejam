'use strict';

appSeedApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .otherwise({
                    templateUrl: 'views/main.html',
                    controller: 'MainController',
                    resolve:{
                        resolvedVendors: ['Vendor', function (Vendor) {
                            return Vendor.query().$promise;
                        }],
                        resolvedVendor: ['Vendor', function (Vendor) {
                            return Vendor.get({id:1}).$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
