'use strict';

appSeedApp.factory('SetPreparing', function ($resource) {
        return $resource(' /app/rest/orders/:id/preparing', {}, {
            'update': { method:'PUT' , params:{id : '@id'}}
        });
    });

appSeedApp.factory('SetReady', function ($resource) {
    return $resource(' /app/rest/orders/:id/ready', {}, {
        'update': { method:'PUT' , params:{id : '@id'}}
    });
});

appSeedApp.factory('SetCollected', function ($resource) {
    return $resource(' /app/rest/orders/:id/collected', {}, {
        'update': { method:'PUT' , params:{id : '@id'}}
    });
});
