'use strict';


appSeedApp.controller('MainController', function ($scope, $location, $rootScope, $filter, resolvedVendors, VendorItem, resolvedVendor, Vendor, Order, VendorOrders, $timeout, SetPreparing, SetReady, SetCollected) {
    //
    // CUSTOMER
    //
        $scope.isCollapsed = true;
        $scope.vendors = resolvedVendors;
        $scope.vendorStores = [];

        $scope.vendorsUnique = function(){
            var unique = [];
            for(var i=0; i< $scope.vendors.length; i++){
                var name = $scope.vendors[i].name;
                var found = false;
                for(var y=0; y < unique.length && unique.length !== 0; y++){
                    var uniqueName = unique[y].name;
                    if(uniqueName === name){
                        found = true;
                    }
                }
                if(!found){
                    unique.push($scope.vendors[i]);
                }
            }
            return unique;
        };

        $scope.vendorsLocations = function(vendorName){

            console.log(vendorName);
            var unique = [];
            for(var i=0; i< $scope.vendors.length; i++){
                if(vendorName === $scope.vendors[i].name){
                    unique.push($scope.vendors[i]);
                };

            }
            $scope.vendorStores = unique;
            console.log($scope.vendorStores);
        };

        $scope.step=1;

        $scope.myInterval = 0;

        $('#cslider').carousel({
            interval: 0
        })

        $scope.goToStep = function(number){
            if(number === 0){
                $scope.items = [];
                $scope.clear();
            }
            $("#cslider").carousel(number);
        }

        $scope.step1 = function(vendor){
            $scope.order.vendor = vendor;
            $("#cslider").carousel('next');
        }

        $scope.step2 = function(category){
            $scope.selectedCategory = category;
            VendorItem.query({vendor: $scope.order.vendor.id, category: category.toUpperCase()}, function(items){
                $scope.items = items;
            })
            $("#cslider").carousel('next');
        }

    $scope.addToCart = function(item){
        var contains = false;
        for(var i=0; i < $scope.order.orderItems.length; i++){
            if($scope.order.orderItems[i].id === item.id){
                contains = true;
                $scope.order.orderItems[i].quantity = $scope.order.orderItems[i].quantity + 1;
                $scope.order.orderItems[i].total = (item.cost * $scope.order.orderItems[i].quantity);
            }
        }

        if(!contains){
            item.quantity = 1;
            item.total = item.cost;
            $scope.order.orderItems.push(item);
        }
    };

    $scope.removeFromCart = function(item){
        for(var i=0; i < $scope.order.orderItems.length; i++){
            if($scope.order.orderItems[i].id === item.id){
                $scope.order.orderItems[i].quantity = $scope.order.orderItems[i].quantity - 1;
                $scope.order.orderItems[i].total = ($scope.order.orderItems[i].cost * $scope.order.orderItems[i].quantity);
                if($scope.order.orderItems[i].quantity === 0){
                    $scope.order.orderItems.splice(i,1);
                }
            }
        }
    };

    $scope.placeOrder = function(user){
        $scope.order.user.login = user.login;

        Order.save($scope.order,
            function (order) {
                $scope.alerts.push({type: 'success', msg: 'Order #' + order.id + ' succesfully created @ ' + order.vendor.name + " - " + order.vendor.location});
                $scope.items = [];
                $scope.clear();
                $location.path('/history');
            }
        );
    }

    $scope.clear = function(){

        //init order
        $scope.order = {
            state: "INQUEUE",
            vendor: {},
            user:{
                login: ''
            },
            ordered: (new Date).getTime(),
            orderItems: []
        }
    }

    $scope.clear();


    //
    // VENDOR
    //
    $scope.vendor = resolvedVendor;



    $scope.orders = {
        inqueue: [],
        preparing: [],
        ready: []
    };

    $scope.pullOrders = function(){
        VendorOrders.query({id: $scope.vendor.id,state: 'INQUEUE'}).$promise.then(function(orders) {
            $scope.orders.inqueue = orders;
        });
        VendorOrders.query({id: $scope.vendor.id,state: 'PREPARING'}).$promise.then(function(orders) {
            $scope.orders.preparing = orders;
        });
        VendorOrders.query({id: $scope.vendor.id,state: 'READY'}).$promise.then(function(orders) {
            $scope.orders.ready = orders;
        });
        $timeout(function(){
            $scope.pullOrders();
        }, 5000);
    }

    $scope.pullOrders();


    $scope.toggleFullScreen = function toggleFullScreen($rootScope) {
        $rootScope.showNav = !$rootScope.showNav;
        if (!document.fullscreenElement &&    // alternative standard method
            !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {  // current working methods
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    };

    $scope.changeState = function (orderId, state, event) {
        if (state === 'inqueue') {
            SetPreparing.update({id: orderId});
        }
        else if (state === 'preparing') {
            SetReady.update({id: orderId});
        }
        else if (state === 'ready') {
            SetCollected.update({id: orderId});
        }

        event.target.parentNode.className = "bg-red";
    };

});
