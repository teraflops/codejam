'use strict';

appSeedApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/orderItem', {
                    templateUrl: 'views/orderItems.html',
                    controller: 'OrderItemController',
                    resolve:{
                        resolvedOrderItem: ['OrderItem', function (OrderItem) {
                            return OrderItem.query().$promise;
                        }],
                        resolvedOrder: ['Order', function (Order) {
                            return Order.query().$promise;
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
