'use strict';

appSeedApp.controller('OrderItemController', function ($scope, resolvedOrderItem, OrderItem, resolvedOrder) {

        $scope.orderItems = resolvedOrderItem;
        $scope.orders = resolvedOrder;

        $scope.create = function () {
            OrderItem.save($scope.orderItem,
                function () {
                    $scope.orderItems = OrderItem.query();
                    $('#saveOrderItemModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.orderItem = OrderItem.get({id: id});
            $('#saveOrderItemModal').modal('show');
        };

        $scope.delete = function (id) {
            OrderItem.delete({id: id},
                function () {
                    $scope.orderItems = OrderItem.query();
                });
        };

        $scope.clear = function () {
            $scope.orderItem = {name: null, description: null, quantity: null, category: null, cost: null, id: null};
        };
    });
