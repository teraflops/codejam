'use strict';

appSeedApp.factory('OrderItem', function ($resource) {
        return $resource('app/rest/orderItems/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
